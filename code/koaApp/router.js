'use strict'

const Router = require('koa-router');

const router = new Router();

async function index(ctx){
    const response = `Welcome To KOA docker demo
    <br>
    Please goto <a href="/hello-docker"> Hello docker page </a>
    `;

    ctx.body = response;
    ctx.set('Content-Type', 'text/html') 
}

router.get('/', index);

async function helloDocker(ctx){
    const nameEnv = process.env.NAME || 'NAME-NOT-FOUND';
    const response = `
     Hello ${nameEnv}
    `
    ctx.body = response;
}

router.get('/hello-docker', helloDocker);

module.exports = router;


