'use strict'

// Koa framework - http://koajs.com/
const koa = require('koa')
const router = require('./router');

const koaBody = require('koa-body')(({
    multipart: true
}))

const app = new koa()

app.use(koaBody)

app.use(router.routes())
app.use(router.allowedMethods())


app.listen(3000)