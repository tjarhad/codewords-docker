## Docker demo using simple KOA App.

* Build koa application docker image

```bash
docker build -t koa-app . 
```

* Run application 

```bash
docker run -it -p 3000:3000 koa-app
```

Goto [localhost:3000](http://localhost:3000)

* RUN container in background

```
docker run -d 3000:3000 koa-app
```

* Pass environment variable : NAME 

```
docker run -p 3000:3000 -e NAME=John  koa-app
```

* Optimize build context

Take a note of output of docker build process

```
$ docker build -t koa-app . 
Sending build context to Docker daemon  4.743MB
```

Uncomment node_modules in `.dockerignore` file

Now, less build context will get sent to docker daemon 

```
$ docker build -t koa-app . 
Sending build context to Docker daemon  28.16kB
```

* Fix build cache 

1. Modify `app.js` and rebuild the docker image
2. Note the npm installation time. 
3. Fix the Dockerfile to leverage build cache

Correct order
```
COPY package*.json ./

RUN npm install

USER node

COPY --chown=node:node . 
```

